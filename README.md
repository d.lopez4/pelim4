# PELIM4
## LINK A LES PAGINES :trophy:
- [x] V0 - [HTML sense estils](https://d.lopez4.gitlab.io/pelim4/v0/) :sob:
- Aqui vaig crear un html amb tota la informació de la pel·licula.
- [x] V1 - [HTML amb estils (CSS compilat automaticament per Sass)](https://d.lopez4.gitlab.io/pelim4/v1/) :sob:
- Al HTML de la V0 li vaig afegir els estils mitjançant Sass per maquetar la pagina.
- [x] V2 - HTML amb plantilla :sob:
    - [x] V2.1 - [Plantilla base (sense text e imatges de la peli)](https://d.lopez4.gitlab.io/pelim4/v2/v2.1/) :sob:
        - Aqui esta la plantilla base descarregada d'una web, sense cap modificació.
    - [x] V2.2 - [Plantilla amb text e imatges de la peli (s'han fet petits canvis al HTML i CSS)](https://d.lopez4.gitlab.io/pelim4/v2/v2.2/) :sob:
        - Amb la plantilla de la V2.1 com a base li vaig afegir tot el contingut de la meva pagina(V0, només text e imatges).
- [x] V3 - [HTML amb bootstrap creat a partir de Froala (editor amb blocs)](https://d.lopez4.gitlab.io/pelim4/v3/) :sob:
- Amb Froala vaig crear una pagina amb blocs i la vaig descarregar com html, després vaig afegir el text e              imatges de la V0.

## 25 PROPIETATS CSS UTILITZADES EN LES PAGINES :game_die:
1. font-family: 'Goldman', cursive;
2. color: white;
3. margin-left: 20%;
4. text-align: center;
5. border-radius: 5px;
6. clear: left;
7. display: flex;
8. flex-wrap: wrap;
9. z-index: 1;
10. background: linear-gradient(27deg, rgba(21,0,36,1) 0%, rgba(114,9,121,1) 35%, rgba(0,129,255,1) 100%);
11. padding: 2vh 2vw;
12. background-color: darkblue;
13. transition-duration: 1s;
14. box-sizing: content-box;
15. border-collapse: collapse;
16. position: sticky;
17. top: 0;
18. text-decoration: none;
19. box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
20. list-style: none;
21. border: 5px groove rebeccapurple;
22. float: left;
23. margin: 2.5%;
24. bottom: 0;
25. width: 20vw;